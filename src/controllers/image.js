const path = require('path');
const {randomNumber} = require('../helpers/libs')
const fs = require('fs-extra');

const {Image} =  require('../models');

const ctlr = {};

ctlr.index = (req, res) => {

};

ctlr.create = async(req, res) => {

    const saveImage = async() =>{
        const imageUrl = randomNumber();
        const images =  Image.find({filename: imageUrl});
        if(images.length>0){
            saveImage();
        }else{
            const imageTempPath = req.file.path;
            const ext = path.extname(req.file.originalname).toLowerCase();
            const targetPath = path.resolve(`public/upload/${imageUrl}${ext}`)
        
            if (ext ==='.png' || ext==='.jpg' || ext==='.jpeg' ||ext==='.gif' ) {
               await fs.rename(imageTempPath,targetPath);
             const newImg = new Image({
                   title:req.body.title,
                   filename:imageUrl + ext,
                   description: req.body.description
               });
               const imageSaved = await newImg.save();
              res.redirect('/images/:image_id');
            }else{
                await fs.unlink(imageTempPath);
                res.status(500).json({error:'Solo imagenes estan permitidas'});
            }
        }
    };
    saveImage();
};

ctlr.like = (req, res) => {

}

ctlr.comment = (req, res) => {

}

ctlr.delete = (req, res) => {

}
module.exports = ctlr;