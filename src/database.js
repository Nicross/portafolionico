const mongose = require('mongoose');

const { database } = require('./keys');

mongose.connect(database.URI, {
    useNewUrlParser: true
})
    .then(db => console.log("DB esta conectada"))
    .catch(err => console.log(err));