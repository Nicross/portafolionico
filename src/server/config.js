const path = require('path');
const exphns = require('express-handlebars');

const morgan = require('morgan');
const multer = require('multer');
const express = require('express');
const errorHanler = require('errorhandler');
const routes = require('../routes/index')

module.exports = app =>{

    app.set('port',process.env.PORT || 3000);
    app.set('views', path.join(__dirname,'../views'));
    app.engine('.hbs',exphns({
        defaultLayout:'main',
        layoutsDir:path.resolve(app.get('views'),'layouts'),
        partialsDir:path.join(app.get('views'),'partials'),
        extname:'.hbs',
        helpers:require('./helpers')
    }));
    app.set('view engine','.hbs');

    //middleward
    app.use(morgan('dev'));
    app.use(multer({
        dest:path.join(__dirname,'../public/upload/temp')
    }).single('image'));

    app.use(express.urlencoded({
        extended:false
    }));

    app.use(express.json());

    //routes
    routes(app);

    //static files
    app.use('/public',express.static(path.join(__dirname,'../public')));

    //error handler
    if('development' === app.get('env')){
        app.use(errorHanler());
    }

    return app;
}